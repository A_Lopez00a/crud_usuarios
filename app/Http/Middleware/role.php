<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $roles = array_slice(func_get_args(), 2);
        
        
        if (!auth()->user()->hasRoles($roles)) {
            abort(403, "No tienes autorización para ingresar");
        }
        return $next($request);
    } 
}
