<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|string',
            'paterno'   => 'required|string',
            'materno'   => 'required|string',
            'username'  => 'required|string',
            'email'     => 'required|string|email',
            'password'  => 'required|string',
        ];
    }

    public function messages()
    {
        return [
            'name.required'      => 'El campo nombre es requerido',
            'paterno.required'   => 'El campo apellido paterno es requerido',
            'materno.required'   => 'El campo apellido materno es requerido',
            'username.required'  => 'El campo username es requerido',
            'email.email'        => 'El correo debe tener un formato correcto',
            'email.required'     => 'El campo correo es requerido',
            'password.required'  => 'El campo contraseña es requerido',
        ];
    }
}
