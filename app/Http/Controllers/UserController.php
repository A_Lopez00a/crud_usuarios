<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Image;
use App\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::orderBy('id','asc')->get();                   
        return view('admin.registrar', compact('roles')); 
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
            try {
                DB::beginTransaction();

                        $result = User::create([
                            'name'     => $request->name,
                            'paterno'  => $request->paterno,
                            'materno'  => $request->materno,
                            'username' => $request->username,
                            'email'    => $request->email,
                            'password' => Hash::make($request['password']),
                            
                        ]); 
                
                            $id_user = $result->id;
 
                                if($request->file('image')){ 
                                    $url = Storage::disk('public')->put('Images',  $request->file('image'));
                                    $result->image()->create([
                                        'url' => $url,
                                        'imageable_id' => $id_user,
                                        'imageable_type' => User::class,
                                    ]);
                                }

                
                                        if($request->roles){
                                            $result->roles()->attach($request->roles); 
                                
                                        }else{
                                            return redirect('/registrar-usuario')->with('status', 'Es necesario asignar un rol');
                                        }

                DB::commit();


                return redirect('home')->with('status', 'Datos guardados con exito!');

            } catch (\Throwable $e) {
                return redirect('home')->with('status', 'Error al gurdar los datos!'.$e); 
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuarios = User::findOrFail($id);         
        $roles = Role::get()->all();   
        return view('admin.editar', compact('usuarios','roles'));  
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        
        try {
                
            $user = User::findOrFail($id);  

                if($request->password != $user->password){
                    $user->update(['password'  => Hash::make($request['password'])   ]);
                }
                    
                    $user->update([
                        'name'      => $request->name,
                        'paterno'   => $request->paterno,
                        'materno'   => $request->materno,
                        'username'  => $request->username,
                        'email'     => $request->email,              
                    ]); 

                        if($request->file('image')){
                            $url = Storage::disk('public')->put('Images', $request->file('image'));

                            if($user->image){
                                Storage::disk('public')->delete($user->image->url);
                                $user->image->update([
                                    'url'   => $url
                                ]);
                            }else{
                                $user->image()->create([
                                    'url' => $url
                                ]);
                            }   
                        }

                                if($request->roles){

                                    $user->roles()->sync($request->get('roles'));
                                    
                                }else{
                                    return redirect('/editar-usuario/'.$id)->with('status', 'Es necesario asignar un rol');
                                }
                                        

                return redirect('home')->with('status', 'Datos actualizados con exito!');

            } catch (\Throwable $e) {
                return redirect('home')->with('status', 'Error al actualizar los datos!'.$e);
            }        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);  
        $result = $user->delete(); 
        
       
        if($user->image){
            $url = str_replace('images','public/images',$user->image->url);
            Storage::delete($url);
            $user->image->delete();     
        }

        if($result){ 
                                       
            return redirect('home')->with('status', 'Usuario eliminado con exito!');
        }else{
                   
            return redirect('home')->with('status', 'Error al eliminar el usuario!');            
        } 
    }
}
