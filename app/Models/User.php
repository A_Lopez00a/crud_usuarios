<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'paterno',
        'materno',
        'username',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles(){
        return $this->belongsToMany(Role::class);
    }

    public function image(){
        return $this->morphOne('App\Models\Image','imageable');
    }
    

    public function contains($id){
    
        $flag = false;
        foreach ($this->roles as $item) {
            if($item->id == $id){
                $flag = true;
            }       
        }
        return $flag;
    }

    public function hasRoles($roles)
    {
        foreach($roles as $role){
            
            foreach($this->roles as $userRole){
                if($userRole->slug == $role){
                    return true;
                }
            }
         
        }
     

        return false;
         
    }

 

}
