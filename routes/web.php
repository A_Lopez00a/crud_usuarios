<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\HomeController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/welcome');
});


//Auth::routes();
Auth::routes(["register" => false]);

Route::middleware('auth')->group(function(){
    Route::get('/home', [HomeController::class, 'index'])->name('home')->middleware('role:administrador,restringido'); 
    Route::get('/registrar-usuario', [UserController::class, 'index'])->name('usuarios.index')->middleware('role:administrador');
    Route::post('/registrar', [UserController::class, 'store'])->name('usuarios.store')->middleware('role:administrador');
    Route::get('/editar-usuario/{id}', [UserController::class, 'edit'])->name('usuarios.edit')->middleware('role:administrador,restringido'); 
    Route::post('/actualizar-usuario/{id}', [UserController::class, 'update'])->name('usuarios.update')->middleware('role:administrador,restringido');
    Route::delete('/eliminar/{id}', [UserController::class, 'destroy'])->name('usuarios.destroy')->middleware('role:administrador');
});
           
 
 