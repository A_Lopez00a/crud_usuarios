@extends('layouts.plantilla')

@section('content')

@if(Session::has('status'))
      <div class="alert alert-success alert-dismissible mr-5 ml-5" role="alert">
          {{Session::get('status')}}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="cerrar">
          <span aria-hidden="true">&times;</span>
        </button>  
      </div>
@endif

<div class="container">

  <div class="card">
    <div class="card-header "><h6 class="font-weight-bold text-secondary">Usuarios registrados</h6></div>
     <div class="card-body">
      <table class="table table-hover" id="usuarios">
        <thead>
          <tr>
            <th scope="col">Imagen</th>
            <th scope="col">Nombre</th>
            <th scope="col">Username</th>
            <th scope="col">Correo</th>
            <th scope="col">Acción</th>
          </tr>
        </thead>
        <tbody>
        @foreach ($usuarios as $item)
              <tr>
                <td>
                  @if ($item->image)
                    <img width="50px" src="{{ Storage::url($item->image->url) }}">                 
                  @else
                      Sin imagen                                        
                  @endif
                </td>
                <td> {{ $item->name }} {{$item->paterno}} {{$item->materno}} </td>          
                <td> {{ $item->username }} </td>          
                <td> {{ $item->email }} </td>
                <td>
                  <a href="{{route('usuarios.edit',$item->id)}}" class="btn btn-link text-warning">
                    <i class="fas fa-user-edit" title="Editar"></i>
                  </a>

                  <form action="{{route('usuarios.destroy', $item->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                      <button type="submit" class="btn btn-link text-danger">
                        <i class="fas fa-user-times" title="Eliminar"></i>
                      </button>
                  </form>

                </td>
              </tr>           
        @endforeach
        </tbody>
      </table>
    </div>
    <div class="m-3">
    
    </div>
  </div> 
</div>

@endsection
