@extends('layouts.plantilla')

@section('content')

<div class="container">

  @if(Session::has('status'))
  <div class="alert alert-warning alert-dismissible mr-5 ml-5" role="alert">
      {{Session::get('status')}}
    
    <button type="button" class="close" data-dismiss="alert" aria-label="cerrar">
      <span aria-hidden="true">&times;</span>
    </button>  
  </div>
@endif

 

  @if ($errors->any())
    <div class="alert alert-warning">
         <ul>
            @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
            @endforeach
         </ul>
    </div>
  @endif



  
    <div class="card m-3">
      <div class="card-body register-card-body ">
        <p class="login-box-msg font-weight-bold">Datos del usuario: <i>{{$usuarios->name}} {{$usuarios->paterno}} {{$usuarios->materno}}</i></p>

       <span class="float-right">
            <a href="{{route('home')}}" class="btn btn-link">
               <h2> <i class="fas fa-arrow-circle-left text-secondary" title="Inicio"></i> </h2>
            </a>
        </span> 

        <div class="card" style="width: 10rem; height:8rem">
            <div class="m-0 row justify-content-center vh-100">
                @if ($usuarios->image)
                <img src="{{ Storage::url($usuarios->image->url) }}" width="150px" height="auto">                 
                @else
                Sin imagen                                          
                @endif
            </div>
        </div>

    
        <form action="{{route('usuarios.update',$usuarios->id)}}" method="post" enctype="multipart/form-data">
          @csrf

          <div class="input-group mb-3">
            <input type="text" class="form-control  @error('name') is-invalid @enderror" name="name" value="{{ $usuarios->name }}" title="Nombre">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>

            @error('name')
                  <span class="invalid-feedback" role="alert">
                       <!-- <strong>{{ $message }}</strong>--></span>
            @enderror 
          </div>


          <div class="input-group mb-3">
            <input type="text" class="form-control @error('paterno') is-invalid @enderror" name="paterno" value="{{ $usuarios->paterno }}" title="Apellido paterno">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>

            @error('paterno')
                  <span class="invalid-feedback" role="alert">
                       <!-- <strong>{{ $message }}</strong>--></span>
            @enderror
          </div>


          <div class="input-group mb-3">
            <input type="text" class="form-control @error('materno') is-invalid @enderror" name="materno" value="{{ $usuarios->materno }}" title="Apellido materno">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>

            @error('materno')
                  <span class="invalid-feedback" role="alert">
                 <!-- <strong>{{ $message }}</strong>--></span>
            @enderror
          </div>


          <div class="input-group mb-3">
            <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ $usuarios->username }}" title="Username">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>

            @error('username')
                  <span class="invalid-feedback" role="alert">
                 <!-- <strong>{{ $message }}</strong>--></span>
            @enderror      
          </div>


          <div class="input-group mb-3">
            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $usuarios->email }}" title="Correo electrónico">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>

            @error('email')
                  <span class="invalid-feedback" role="alert">
                 <!-- <strong>{{ $message }}</strong>--></span>
            @enderror
          </div>


          <div class="input-group mb-3">
            <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="password" value="{{$usuarios->password}}" name="password" title="Contraseña">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>

            @error('password')
                  <span class="invalid-feedback" role="alert">
                 <!-- <strong>{{ $message }}</strong>--></span>
            @enderror
          </div> 

          <div class="input-group mb-3"> 
            <input type="file" name="image" id="image" class="form-control" title="Actualizar foto de perfil">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-image"></span> 
              </div>
            </div>
          </div> 

          @if(auth()->user()->hasRoles(['administrador']))
            <hr class="bg-info">
            <h5 class="login-box-msg font-weight-bold">Roles asignados</h5> 
            <div class="m-0 row justify-content-center">
              <div class="col-auto text-center">

                  @foreach ($roles as $role => $item)                               
                      <label class="inline-flex items-center">  
                          <input type="checkbox" id="{{ $item->name }}" class="form-checkbox" name="roles[]" title="Asigne un rol" value="{{ $item->id}}" {{ $usuarios->contains($item->id)  == true ? 'checked' : '' }}>                                            
                          <span class="ml-2">{{ $item->name }}</span>      
                      </label>                
                  @endforeach 
          
              </div>
            </div>
            <hr class="bg-info">
          @endif

          <div class="row float-right">

              <button type="submit" class="btn btn-outline-info">Actualizar</button>
      
          <div class="col-12">
              
            
        </form>
  
      </div>
      <!-- /.form-box -->
    </div><!-- /.card -->


@endsection