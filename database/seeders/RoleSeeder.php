<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name'          => "Administrador",
            'slug'          => "administrador",
            'description'   => "Rol que permite manejo total del sistema",
        ]);

        Role::create([
            'name'          => "Restringido",
            'slug'          => "restringido",
            'description'   => "Rol que no permite el total acceso al sistema",
        ]);

        Role::create([
            'name'          => "Sin acceso",
            'slug'          => "sin acceso",
            'description'   => "Rol para baja temporal",
        ]);
    }
}
