<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Image;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        User::create([
            'name'      => "Adrian",
            'paterno'      => "Lopez",
            'materno'      => "Valdes",
            'username'      => "Admin",
            'email'     => "admin@gmail.com",
            'password'  => bcrypt('aaaaaaaa')
        ]);

        Image::factory(1)->create([
            'imageable_id' => '1',
            'imageable_type' => User::class,
        ]);

        $user = User::find(1);
        $user->roles()->attach(1);

                  
        $users = User::factory(50)->create(); 
        foreach ($users as $user) {
            Image::factory(1)->create([
                'imageable_id'      => $user->id,
                'imageable_type'    => User::class
            ]);
            $user->roles()->attach([
                rand(2,3),
                
            ]);
        } 

    }
}
